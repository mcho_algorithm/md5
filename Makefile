all: main.o md5.o
	gcc main.o md5.o -o md5
main.o : main.c md5.h
	gcc -c main.c
md5.o : md5.c
	gcc -c md5.c


clean:
	rm md5 *.o
